# Robot Head
Copyright (c) 2018 Bart Massey

This is (will be) a piece of software designed to control a
cardboard-box robot head for human wear.

Planned Robot Head features include:

* Microphone and powered speaker for "robot voice".

* Light up eyes driven by voice.

* Muffin fan swamp cooler.

Target hardware is some very cheap ARM
microcontroller. Target software is bare-metal Rust.

# License

This work is licensed under the GPL version 3 or later. See
the file `LICENSE` in this distribution for license terms.
